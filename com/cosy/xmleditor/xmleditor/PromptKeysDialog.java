package com.cosy.xmleditor.xmleditor;

import java.awt.GridLayout;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.Map;

import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JTextField;

public class PromptKeysDialog extends JFrame{
	private String newKey;
	Map<String, String> map;
	
	JTextField input;
	JList<String> result;
	
	public PromptKeysDialog(String s,Map<String,String> map) {
		super(s);
		this.map = map;
		
		input = new JTextField();
		result = new JList<>();
		
		input.addKeyListener(new keyListener());
		setSize(400, 400);
		getContentPane().setLayout(new GridLayout(2,1,1,1));
		getContentPane().add(input);
		getContentPane().add(result);
	}
	public static String showDialog(){
		
		return "";
	}
	private class keyListener extends KeyAdapter{
		@Override
		public void keyPressed(KeyEvent e) {
			System.out.println(input.getText());
		}
	}
	public String getNewKey() {
		return newKey;
	}

	public void setNewKey(String newKey) {
		this.newKey = newKey;
	}
}
