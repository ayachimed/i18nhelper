/*
 * Copyright 2006-2008 Kees de Kooter
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.cosy.xmleditor.xmleditor;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextPane;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;

/**
 * Frame displaying the XML textpane.
 *
 * @author kees
 *
 */
public class XmlEditor extends JFrame {

	private static final long serialVersionUID = 2623631186455160679L;
	XmlTextPane xmlTextPaneLeft;
	JTextPane xmlTextPaneRight;
	private PromptKeysDialog prompt;
	static Map<String, String> map = new HashMap<>();
	static{
		loadProperties();
	}
	public static void main(String[] args) {
		new XmlEditor();
	}
	private static void loadProperties() {
		try (BufferedReader br = new BufferedReader(new FileReader(new File("E:/i18n/Language.properties")))) {
		    String line = "";
		    while ((line = br.readLine()) != null) {
		       if(!(line.startsWith("#") ||  line.equals("") || line.split("=").length == 0)){
		    	   map.put(line.split("=")[0], line.split("=")[1]);
		       }
		       
		    }
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public XmlEditor() {

		super("XHTML Text Editor");
		setSize(800, 600);
		setPrompt(new PromptKeysDialog("Search keys", map));

		JPanel panel = new JPanel();
		panel.setLayout(new GridLayout(1, 2, 1, 1));

		
		
		xmlTextPaneLeft = new XmlTextPane(this);
		JScrollPane jScrollPaneLeft = new JScrollPane(xmlTextPaneLeft);
		panel.add(jScrollPaneLeft);
		
		
		xmlTextPaneRight = new JTextPane();
		JScrollPane jScrollPaneRight = new JScrollPane(xmlTextPaneRight);
		panel.add(jScrollPaneRight);
		
		
		JSplitPane splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT,
				jScrollPaneLeft, jScrollPaneRight);
		
        splitPane.setOneTouchExpandable(true);
        splitPane.setResizeWeight(0.5);
        add(splitPane, BorderLayout.CENTER);
        
		setExtendedState(JFrame.MAXIMIZED_BOTH);
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	public void addEntry(String key, String entry) {
		StyledDocument doc = xmlTextPaneRight.getStyledDocument();

		// Define a keyword attribute

		SimpleAttributeSet keyWord = new SimpleAttributeSet();
		StyleConstants.setForeground(keyWord, Color.RED);
		StyleConstants.setBackground(keyWord, Color.YELLOW);
		StyleConstants.setBold(keyWord, true);

		// Add some text
		try {
			
			doc.insertString(doc.getLength(), key+" = "+entry+"\n", keyWord);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public PromptKeysDialog getPrompt() {
		return prompt;
	}
	public void setPrompt(PromptKeysDialog prompt) {
		this.prompt = prompt;
	}
}
