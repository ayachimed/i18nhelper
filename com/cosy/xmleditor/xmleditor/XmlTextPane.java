/*
 * Copyright 2006-2008 Kees de Kooter
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cosy.xmleditor.xmleditor;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JOptionPane;
import javax.swing.JTextPane;
import javax.swing.text.BadLocationException;

/**
 * JTextPane implementation that can handle xml text. The IndentKeyListener
 * implements smart indenting.
 * 
 * @author kees
 * @date 27-jan-2006
 *
 */
public class XmlTextPane extends JTextPane {

	private static final long serialVersionUID = 6270183148379328084L;
	private Logger logger = Logger.getLogger(getClass().getName());
	private XmlEditor xmlEditor;
	String textToSelect = "";

	public XmlTextPane(final XmlEditor xmlEditor) {
		this.setXmlEditor(xmlEditor);
		// Set editor kit
		this.setEditorKitForContentType("text/xml", new XmlEditorKit());
		this.setContentType("text/xml");

		addKeyListener(new IndentKeyListener());
		addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				if (getSelectedText() != null) {
					String mapKey = getMapKeyForValue(XmlEditor.map, getSelectedText());
					String key = "";
					if (mapKey.equals("")) {
						key = JOptionPane.showInputDialog(null, "Enter key");

					} else {
						key = JOptionPane.showInputDialog("Enter key", mapKey);
					}
					// replace selected text
					if (key != null) {
						textToSelect = getSelectedText().replaceAll("�", "é").replaceAll("�", "ê");
						if (mapKey.equals("")) {
							xmlEditor.addEntry(key, textToSelect);
							XmlEditor.map.put(key, textToSelect);
						}

						replaceSelection("#{i18n['" + key + "']}");
						checkForOtherOccurences(key);
					}

				}
			}

		});
	}
	private void checkForOtherOccurences(String key) {
		int index = getText().indexOf(textToSelect);
		while(index >= 0) {
			select(index,index+textToSelect.length());
			
			if(JOptionPane.showConfirmDialog(xmlEditor, "Replace","Replace this occurence!",JOptionPane.YES_NO_OPTION) == 0){
				replaceSelection("#{i18n['" + key + "']}");
			}
		    index = getText().indexOf(textToSelect, index+textToSelect.length()+1);
		    
		}
	}
	private String getMapKeyForValue(Map<String, String> map, String value) {
		for (String key : map.keySet()) {
			if (map.get(key).trim().equalsIgnoreCase(value.trim())) {
				return key.trim();
			}
		}
		return "";
	}

	public XmlEditor getXmlEditor() {
		return xmlEditor;
	}

	public void setXmlEditor(XmlEditor xmlEditor) {
		this.xmlEditor = xmlEditor;
	}

	private class IndentKeyListener implements KeyListener {

		private boolean enterFlag;
		private final Character NEW_LINE = '\n';

		public void keyPressed(KeyEvent event) {
			enterFlag = false;
			if ((event.getKeyCode() == KeyEvent.VK_ENTER) && (event.getModifiers() == 0)) {
				if (getSelectionStart() == getSelectionEnd()) {
					enterFlag = true;
					event.consume();
				}
			}
		}

		public void keyReleased(KeyEvent event) {
			if ((event.getKeyCode() == KeyEvent.VK_ENTER) && (event.getModifiers() == 0)) {
				if (enterFlag) {
					event.consume();

					int start, end;
					String text = getText();

					int caretPosition = getCaretPosition();
					try {
						if (text.charAt(caretPosition) == NEW_LINE) {
							caretPosition--;
						}
					} catch (IndexOutOfBoundsException e) {
					}

					start = text.lastIndexOf(NEW_LINE, caretPosition) + 1;
					end = start;
					try {
						if (text.charAt(start) != NEW_LINE) {
							while ((end < text.length()) && (Character.isWhitespace(text.charAt(end)))
									&& (text.charAt(end) != NEW_LINE)) {
								end++;
							}
							if (end > start) {
								getDocument().insertString(getCaretPosition(), NEW_LINE + text.substring(start, end),
										null);
							} else {
								getDocument().insertString(getCaretPosition(), NEW_LINE.toString(), null);
							}
						} else {
							getDocument().insertString(getCaretPosition(), NEW_LINE.toString(), null);
						}
					} catch (IndexOutOfBoundsException e) {
						try {
							getDocument().insertString(getCaretPosition(), NEW_LINE.toString(), null);
						} catch (BadLocationException e1) {
							logger.log(Level.WARNING, e1.toString());
						}
					} catch (BadLocationException e) {
						logger.log(Level.WARNING, e.toString());
					}
				}
			}
		}

		public void keyTyped(KeyEvent e) {
		}
	}

}
